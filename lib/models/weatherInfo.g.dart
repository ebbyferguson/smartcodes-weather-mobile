// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'weatherInfo.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WeatherInfo _$WeatherInfoFromJson(Map<String, dynamic> json) {
  return WeatherInfo(
    temperature: (json['temperature'] as num)?.toDouble(),
    description: json['description'] as String,
    cityName: json['cityName'] as String,
    icon: json['icon'] as String,
    wind: (json['wind'] as num)?.toDouble(),
    humidity: (json['humidity'] as num)?.toDouble(),
    pressure: (json['pressure'] as num)?.toDouble(),
    des: json['des'] as String,
  );
}

Map<String, dynamic> _$WeatherInfoToJson(WeatherInfo instance) =>
    <String, dynamic>{
      'temperature': instance.temperature,
      'description': instance.description,
      'cityName': instance.cityName,
      'icon': instance.icon,
      'wind': instance.wind,
      'humidity': instance.humidity,
      'pressure': instance.pressure,
      'des': instance.des,
    };
